# [De 0 a 2000 amb el tito TaToH](https://www.youtube.com/playlist?list=PLB1AJxrCNNIMexMr-xZRNlTPp7hr4PJjE)

Notes personals per no haver de mirar aquests vídeos kilomètrics cada vegada que vulgui refrescar la ageologia.

## [Ep 1: Empezando](https://youtu.be/DMCQPL9NKYE)

### Les Regles d'or de l'Age2

1. Tenir els Town Centers parint vilatans constantment.

2. Tenir als vilatans ocupats constantment.

3. Gastar els recursos, no acumular-los.

### Timings

Un "timing" és una finestra de 30 segons.


## [Ep 2: Alta Edad Media](https://youtu.be/3mesz9CscR4)

### Build Order

| Total | Vilatans | Tasca                        |
| -----:|---------:| -----------------------------|
| 6     | 6        | Ovelles                      |
| 10    | 4        | Fusta (+lumber camp)         |
| 11    | 1        | Caçar senglar 1 -> ovelles   |
| 12    | 1        | Casa, Molí i arbustos        |
| 15    | 3        | Arbustos                     |
| 16    | 1        | Caçar senglar 2 i granja     |
| 17    | 1        | Casa i granja                |
| 20    | 3        | Senglar 2                    |
| 23    | 3        | Fusta, lumber camp 2         |

A l'últim pas s'acumulen molts vilatans al senglar/ovelles.
S'han d'anar transicionant a la fusta.

### Observacions

Els lumber camps van enganxats als boscos.

És millor crear 2 lumber camps i repartir els vilatans.

Els Molins van enganxats als arbustos.

El menjar de caça és el més ràpid de recollir.

Un bon moment per fer els Barracks és pujant a Feudal.

### Reptes

Arribar a Feudal en 12 minuts o menys.


## [Ep 3: Explorando la Edad Feudal](https://youtu.be/T84WToW0tlw)

### Observacions

Les granjes s'han d'anar construint paulatinament per anar gastant la fusta i que **no s'esgotin totes de cop**.

Pujar a Feudal són 4 "timings" i mig (2 minuts i mig).

A Dark Age cada vilatà produeix 10 recursos per "timing", amb això es pot calcular aproximadament quants recursos guanyarem mentre pugem d'edat.

El recurs més important és la **fusta** perquè és la més versàtil.

La primera millora del Lumber Camp [Double-Bit Axe](https://ageofempires.fandom.com/wiki/Double-Bit_Axe) s'ha de fer immediatament al pujar a Feudal.

La primera millora del Molí [Horse Collar](https://ageofempires.fandom.com/wiki/Horse_Collar) surt a compte si volem centrar-nos en produïr menjar a Feudal (p. e. Scouts o MAA).

El [Wheelbarrow](https://ageofempires.fandom.com/wiki/Wheelbarrow_(Age_of_Empires_II)) també s'ha de fer a Feudal i es nota més a les granjes (+12/14 granjes o +30/35 vilatans, s'ha de prioritzar segons la producció).

No té sentit posar-se a minar or d'entrada si a Feudal et vols centrar en fer menjar.

Una bona manera de practicar hotkeys és amb el mode Death Match, on comences amb un porró de recursos.


## [Ep 4: Reto de los 5 minutos](https://youtu.be/Reln01VeCQ4)

Episodi chorra.
El repte consisteix en veure quina població tens al cap de 5 minuts en mode Death Match, sense restriccions de recursos.
Ell en fa 84.

### Observacions

Quan poses 2 vilatans a construir un edifici no s'aixeca el doble de ràpid.
Per exemple: quatre vilatans van el doble de ràpid que 1 vilatà a construir els Barracks, no dos.
Això implica que si hi han molts edificis per fer surt més a compte escampar els vilatans que concentrar-los en els mateixes edificis.

## [Ep 5: Entendiendo la importancia de la herrería](https://youtu.be/VckuQ8-lovc)

### Unitats de Feudal

| Unitats    | HP    | Atac | Armadura | Bonificació Atac       |
|------------|------:|-----:|---------:|------------------------|
|Men-at-Arms | 45    | 6    | 0 / 1    | Cap                    |
|Spearman    | 45    | 3    | 0 / 0    | +15 Cavalleria         |
|Scout       | 45    | 5    | 0 / 2    | Cap                    |
|Skirmisher  | 30    | 2    | 0 / 3    | +3 Llancers i Arquers  |
|Archer      | 30    | 4    | 0 / 0    | +3 Llancers            |

L'armadura resta a l'atac. Una millora de +1 a l'atac o armadura pot suposar una millora determinant en certes situacions.

### Millores Blacksmith

En general les millores més importants segons el tipus d'unitat són:

* Arquers: [Fletching](https://ageofempires.fandom.com/wiki/Fletching) (atac i abast)

* Unitats de cos a cos: Armadura (de infanteria o cavalleria, segons el cas)

Abans d'invertir en cap millora val la pena descobrir quines unitats té el contrincant i quines millores ha fet.

Contra vilatans desprotegits s'ha d'atacar ràpid sempre.

### Exemple de càlcul amb unitats de tir

Els arquers tenen 4 d'atac, i els skirms 3 de defensa de tir.
Per tant un tret d'arquer només li treu 1 punt de vida a un skirm.
Però amb Fletching els arquers tenen +1 d'atac, per tant farien 2 de mal.
Sembla una tonteria però és una millora del 100%: ((2/1) - 1) * 100.
Això implica que n'hi hauria prou amb la meitat d'arquers que abans per baixar un grup de skirms (situació molt desfavorable de tota manera).

### Exemple de càlcul amb unitats cos a cos

Un Scout té 45 punts de vida i 0 de defensa cos a cos.
Un Man-At-Arms té 6 punts d'atac, per tant se'l baixa en 45/6 = 7.5 -> 8 hòsties.
Millorant l'armadura de cavalleria guanya un punt de defensa i passa a perdre 5 punts, per tant ara caldrien 9 hòsties i pot durar una mica més.

Per contra, si davant tenim un Llançer (3+15 d'atac) l'armadura no ajuda en res (45/18 -> 3 hòsties, 45/17 -> 3 hòsties).


## [Ep 6: Balanceando nuestra economía](https://youtu.be/bXPbFICXuSI)

### Consells econòmics

Els vilatans descarreguen qualsevol recurs que portin si construeixen un edifici econòmic (TC, Molí, Granja, Lumber Camp i Mining Camp).

Els Mining Camps es col·loquen a un espai de distància per que els vilatans no xoquin si treballen més de 4. 
Si només hi han de treballar 4 vilatans o menys es poden col·locar enganxats al recurs i no és tant greu (perquè no n'hi han prous com per molestar-se entre ells).

El punt de trobada del Town Center a la fusta (és el recurs més versàtil i útil).

Si tenim vilatans desocupats i no sabem on assignar-los, enviar-los a la fusta.

Si tenim molta fusta, enviar vilatans de fusta a granjes.

Si ens quedem sense marge de població, és un bon moment per investigar una tecnologia del TC com [Loom](https://ageofempires.fandom.com/wiki/Loom) o [Wheelbarrow](https://ageofempires.fandom.com/wiki/Wheelbarrow_(Age_of_Empires_II)) mentre construïm les cases.

### Consells militars

Assignar els vilatans als recursos en funció del nostre objectiu.
Aproximadament, cal un vilatà per cada 10 recursos que costi cada unitat.

1. Scouts. Necessitarem 8 vilatans a granjes per cada Stable (+8 a granjes per mantenir la producció de vilatans).

2. Archers. Necessitarem 3 vilatans a fusta i 4 a or per cada Archery Range.

3. Men-At-Arms. Necessitarem 6 vilatans a granjes i 2 a or per cada Barracks (+8 a granjes per mantenir la producció de vilatans).

Si l'exèrcit està atacant la base enemiga, cal apartar les unitats a una zona segura abans de tornar a centrar la nostra atenció a l'economia de la base.
A aquesta tècnica el TaToH li diu fer un "moviment defensiu".


## [Ep 7: Reto de Arqueros](https://youtu.be/y1_3En8u9Xc)

El repte consisteix en treure el màxim d'arquers al minut 18.
Ell en fa 18, i diu que un novato com a mínim n'hauria de poder fer 10.

### Observacions

Amb els arquers millorats amb Fletching i els vilatans millorats amb Loom calen 14 arquers per baixar-los d'un tret (pre Logistics, si cap arquer falla el tret).


## [Ep 8: Cómo posicionar nuestros edificios](https://youtu.be/P8rsKYiSvu8)

### Tancant la base

La base es tanca construint edificis no econòmics (cases, palise walls, Barracks, Arch Ranges, Blacksmith, Market, etc) al voltant de la base i entre els boscos, a mode de mur:

* La base s'ha d'anar tancant poc a poc.

* Les primeres parts a tancar i protegir és on hi ha la fusta, l'or i la pedra, en aquest ordre.

* Es més eficient i fàcil defensar dos recursos alhora si estan a prop.

### Zones de control

Els molt important controlar els turons que hi ha al voltant de la nostra base [(per la bonificació d'atac i defensa si hi ha desnivell)](https://youtu.be/ooBkMc9pVwM), sobre tot si està entre la nostra base i la de l'enemic.
Per controlar els turons, s'hi pot construir edificis militars o posant el punt de reunió militar a la part més alta.


## [Ep 9: Puliendo detalles](https://youtu.be/fR2pJiscJMk)

### Torres

Les torres protegeixen la base però endarrereixen l'economia (els vilatans han re recollir pedra per construir les torres en comptes d'altres recursos i edificis).

Si la base està ben protegida l'exèrcit haurà d'atacar.

Les torres de vigilància són útils per protegir:

1. Recursos escassos i llunyans com l'or o la pedra.

2. La base quan és difícil tancar-la, sobre tot en mapes molt oberts, amb pocs boscos o turons que l'enemic pugui aprofitar. 

Les torres s'han de posicionar: 

1. A prop dels recursos i vilatans (per que es puguin aixoplugar ràpid a la torre).

2. Orientades cap a la base enemiga (per veure abans quan s'acosta l'enemic.

Les torres son fortes contra arquers i dèbils contra cavallers i vilatans (tenen bonificació d'atac contra edificis).

Les torres no ataquen a unitats que estan a sota fins a Castle Age i s'investiga [Murder Holes](https://ageofempires.fandom.com/wiki/Murder_Holes) (a la Universitat). Per protegir les torres es poden construir palisade walls a la base.

### Man-at-arms

Són unitats molt fortes si són moltes (els Scouts són més maniobrables i els Arquers els fan la contra).
La contrapartida és que també són la unitat feudal més lenta i se li escapen tant els vilatans com altres unitats militars que no vulguin lluitar.

### Town Watch

Ajuda molt a tenir visió de mapa.
Si et tanques bé no és indispensable però al ser barateta va bé tenir-la.

### Observacions sobre el Fast Castle

Una Feudal Age consolidada li fa la contra a un Fast Castle enemic (tindrà menys vilatans, la seva economia serà més dèbil, no tindrà prou exèrcit...).

Si queda poc per pujar a Castle Age cal evitar el combat fins que millorem les unitats.


## [Ep 10: Terminando las teclas](https://youtu.be/1J9DQjR3pX4)

Es poden descarregar la plantilla de hotkeys que fa servir el TaToH [aqui](https://mega.nz/file/sEYDlL5A#eQDR7J5aG1QujsnGkTDGf4kDGGjGGMCuGJxqgDq561A).

No hi ha massa més a comentar, parlar de hotkeys és un puto conyàs.


## [Ep 11: Llegando a castillos con arqueros](https://youtu.be/UPua_y3VIeM)


## [Ep 12: Llegando a castillos con scouts](https://youtu.be/gRSY0raqwRo)


## [Ep 13: Cómo actuar si pierdes todo el ejército antes de Castillos](https://youtu.be/YWONBGYTnfI)

Vídeo molt interessant on li focken tot l'exèrcit a feudal en una escaramussa i es para un atac amb arquers que venia a petar-li la base.
L'atac coincideix amb la seva pujada a castells i ensenya els counters de castells.

### Observacions

Si el contrincant porta Scouts diu que es paren amb un Monestir (??? devia pensar en Knights), però no ensenya la tècnica al vídeo.

Si el contrincant porta arquers el counter és una catapulta, però cal saber micromanejar-la bé.

La tècnica del micromaneig amb catapulta consisteix en disparar manualment i fer un moviment lateral immediatament.
Repeat and repeat fins que la petin.

Si l'enemic també porta catapultes cal procurar tenir-ne una més que ell.

Si t'és humanament possible combinar la defensa amb petites incursions a la base enemiga és fàcil pillar el contrincant amb la guàrdia baixa en aquestes situacions.


## [Ep 14: Pase directo a castillos o FC](https://youtu.be/MzvC9yXvMqo)

Demostra el build order per a un Fast Castle amb 29+2 vilatans, que completa en 17 minuts.

| Total | Vilatans | Tasca                        |
| -----:|---------:| -----------------------------|
| 6     | 6        | Ovelles                      |
| 10    | 4        | Fusta (+lumber camp)         |
| 11    | 1        | Caçar senglar 1 -> ovelles   |
| 12    | 1        | Casa, Molí i arbustos        |
| 15    | 3        | Arbustos                     |
| 16    | 1        | Caçar senglar 2 i granja     |
| 18    | 2        | Casa i ovelles/senglars      |
| 23    | 5        | 2n lumber camp, casa         |
| 26    | 3        | Fusta aprop del TC, granges  |
| 29    | 3        | Mining Camp, or              |

Quan quedi poc menjar sota el Town Center, enviar 4 vilatans als cèrvols
i fer un molí allà mateix. Agafar-ne 2 més i enviar-ne un a cada
lumber camp, per fer un total de 10 llenyataries repartits
entre els 2 camps. La resta que quedin sota el Town Center (6) escurant la caça
ha de fer granges quan se l'acabin.

### Mentre es puja a feudal

Agafar un vilatà dels arbustos i fer una casa i la caserna.

### A feudal

Fer immediatament l'estable i el blacksmith, i fer 2 vilatans més per a l'or.

Acaba amb una distribució de vilatans que el deixa molt just de fusta (insisteix en
deixar el punt de reunió a la fusta a partir d'ara) però que li permet anar traient
cavalleria pesada constantment dels 2 estables que ha construit (el 2n pujant a castells).

### Distribució final a castells

| Tasca        | Quantitat |
|--------------| ---------:|
| Miners       | 8         |
| Llenyataires | 12        |
| Grangers     | 12        |

### Observacions

Jugant de central recomana anar sempre a cavalleria pesada, per la velocitat i perquè al fer
boom la economia ho suporta millor.

En comptes d'apretar tant amb els estables i la cavalleria pesada es pot estalviar la fusta del
segon estable i deixar més vilatans a la fusta, això permet boomejar més ràpid (fer el 2n TC abans).

## [Ep 15: Apertura con Chinos](https://youtu.be/LaDlJfpbFkw)

Els xinesos s'obren diferent perquè comencen amb 6 vilatans i sense menjar (lol).

Amb 1 fas una casa i amb la resta explores i fas el telar immediatament.

Al començar a 0 de menjar cal estar carregant i descarregant menjar constantment
per no tenir el TC parat. També es pot construïr abans el molí que el lumber camp
per donar aquest boost al menjar (però només amb 3 vilatans als arbustos).

## [Ep 16: Aprendiendo a nadar](https://youtu.be/fnjltfJh-4Y)


## [Ep 17: Jugando agua en feudal](https://youtu.be/Uiu09dzn6wM)


## [Ep 18: Nómada](https://youtu.be/Kcm8U-pwqn4)


## [Ep 19: Boom de pesca](https://youtu.be/Te4_yi8ovb4)


## [Ep 20: Optimizando la Alta Edad Media](https://youtu.be/MJf8jS6X4Ns)


## [Ep 21: Tecnologías del Castillo y Universidad](https://youtu.be/u2_lw1QCXiA)


## [Ep 22: Micromanejo básico](https://youtu.be/8vKM47WhBwM)


## [Ep 23: El poder del wololo](https://youtu.be/HxZI-asryL4)


## [Ep 24: Jugar de lateral en equipo](https://youtu.be/KhbwdE-_DRo)


## [Ep 25: Algunas ideas para jugar de central](https://youtu.be/UJszmrIDc8k)


## [Ep 26: Rush de Torres o trush](https://youtu.be/-KMAOiAIIl8)


## [Ep 27: Hombres de Armas y agresivo](https://youtu.be/liJjpgcZm2c)


## [Ep 28: Hombres de Armas y defensa](https://youtu.be/WIdrukmKr3w)


## [Ep 29: Drush y castillos](https://youtu.be/Jjl0MQ4xTls)


## [Ep 30: Drush y pase rápido a Feudal](https://youtu.be/GUgDlYL2Wg4)


## [Ep 31: FC y priorizar economía](https://youtu.be/V1jekAPI-RM)


## [Ep 32: FC y priorizar lo militar](https://youtu.be/DQwS-8XqBks)


## [Ep 33: Pase rápido a Castillos y Castillo](https://youtu.be/WlEaz-nRdMk)


## [Ep 34: Exploración básica](https://youtu.be/_gmlx9C3Whs)


## [Ep 35: Los Hunos](https://youtu.be/039E9hmIpKE)


## [Ep 36: Los Indios](https://youtu.be/EZo42HJRfxA)


## [Ep 37: Los Coreanos](https://youtu.be/pV1kEqC3K14)


## [Ep 38: Apertura de scouts avanzada](https://youtu.be/mRRKNTXg9Iw)


## [Ep 39: Apertura avanzada en agua](https://youtu.be/55oDVx_v57k)
